import inquirer from "inquirer";

async function main() {
  console.log("Welcome to the CLI bar 🍸");
  const input = await inquirer.prompt([
    {
      type: "input",
      name: "drinkInput",
      message: "What drink would you like to order?",
      default: "Margherita",
    },
  ]);

  console.log(`You ordered "${input.drinkInput}"`);
}

main();
